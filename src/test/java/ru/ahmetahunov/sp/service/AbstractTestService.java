package ru.ahmetahunov.sp.service;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.ahmetahunov.sp.config.DatabaseTestConfig;
import ru.ahmetahunov.sp.config.ServiceTestConfig;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {DatabaseTestConfig.class, ServiceTestConfig.class})
public class AbstractTestService {
}
