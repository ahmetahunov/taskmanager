package ru.ahmetahunov.sp.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import static org.junit.Assert.*;

public class UserDetailsServiceBeanTest extends AbstractTestService {

	@Autowired
	private UserDetailsService userDetailsService;

	@Test
	public void loadUserByUsername() {
		assertNotNull(userDetailsService.loadUserByUsername("admin"));
		assertNotNull(userDetailsService.loadUserByUsername("user"));
	}

	@Test(expected = UsernameNotFoundException.class)
	public void loadUserByUsernameUnknownUserFailed() {
		userDetailsService.loadUserByUsername("unknown");
	}

	@Test(expected = UsernameNotFoundException.class)
	public void loadUserByUsernameNullUserFailed() {
		userDetailsService.loadUserByUsername(null);
	}

}