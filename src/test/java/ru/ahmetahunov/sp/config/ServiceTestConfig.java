package ru.ahmetahunov.sp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;
import ru.ahmetahunov.sp.api.service.IProjectService;
import ru.ahmetahunov.sp.api.service.ITaskService;
import ru.ahmetahunov.sp.api.service.IUserService;
import ru.ahmetahunov.sp.service.ProjectService;
import ru.ahmetahunov.sp.service.TaskService;
import ru.ahmetahunov.sp.service.UserDetailsServiceBean;
import ru.ahmetahunov.sp.service.UserService;

@Configuration
public class ServiceTestConfig {

	@Bean
	public IUserService userService() {
		return new UserService();
	}

	@Bean
	public IProjectService projectService() {
		return new ProjectService();
	}

	@Bean
	public ITaskService taskService() {
		return new TaskService();
	}

	@Bean
	public UserDetailsService userDetailsService() {
		return new UserDetailsServiceBean();
	}

}
