package ru.ahmetahunov.sp.integration;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.ahmetahunov.sp.client.ProjectClient;
import ru.ahmetahunov.sp.client.TaskClient;
import ru.ahmetahunov.sp.dto.ProjectDTO;
import ru.ahmetahunov.sp.dto.TaskDTO;

public class TaskRestIT extends AbstractRest {

	private static ProjectDTO project;

	@BeforeClass
	public static void initProject() {
		project = new ProjectDTO();
		project.setName("test");
		@Nullable final ProjectDTO created = projectClient.addProject(userHeader, project);
		Assert.assertNotNull(created);
		Assert.assertEquals(project.getId(), created.getId());
	}

	@AfterClass
	public static void removeProject() {
		projectClient.removeProject(userHeader, project.getId());
		Assert.assertNull(projectClient.getProject(userHeader, project.getId()));
	}

	@After
	public void clean() {
		for (@NotNull final TaskDTO taskDTO : taskClient.getTasks(userHeader)) {
			taskClient.removeTask(userHeader, taskDTO.getId());
		}
	}

	@Test
	public void addTaskTest() {
		@NotNull final TaskDTO taskDTO = new TaskDTO();
		taskDTO.setProjectId(project.getId());
		taskDTO.setName("test");
		@Nullable final TaskDTO created = taskClient.addTask(userHeader, taskDTO);
		Assert.assertNotNull(created);
		Assert.assertEquals(taskDTO.getId(), created.getId());
		Assert.assertEquals(taskDTO.getName(), created.getName());
		taskClient.removeTask(userHeader, created.getId());
		Assert.assertNull(taskClient.getTask(userHeader, created.getId()));
	}

	@Test
	public void updateTaskTest() {
		@NotNull final TaskDTO taskDTO = new TaskDTO();
		taskDTO.setProjectId(project.getId());
		taskDTO.setName("test");
		@Nullable final TaskDTO created = taskClient.addTask(userHeader, taskDTO);
		Assert.assertNotNull(created);
		created.setName("test2");
		created.setDescription("description");
		@Nullable final TaskDTO updated = taskClient.updateTask(userHeader, created);
		Assert.assertNotNull(updated);
		Assert.assertEquals(created.getName(), updated.getName());
		Assert.assertEquals(created.getDescription(), updated.getDescription());
		Assert.assertNotNull(taskClient.getTask(userHeader, created.getId()));
		taskClient.removeTask(userHeader, created.getId());
		Assert.assertNull(taskClient.getTask(userHeader, created.getId()));
	}

	@Test
	public void removeTaskTest() {
		@Nullable final TaskDTO taskDTO = new TaskDTO();
		taskDTO.setProjectId(project.getId());
		taskDTO.setName("test");
		Assert.assertNotNull(taskClient.addTask(userHeader, taskDTO));
		taskClient.removeTask(userHeader, taskDTO.getId());
		Assert.assertNull(taskClient.getTask(userHeader, taskDTO.getId()));
	}

	@Test
	public void getTasksTest() {
		@NotNull final TaskDTO taskDTO = new TaskDTO();
		taskDTO.setProjectId(project.getId());
		taskDTO.setName("test");
		@NotNull final TaskDTO taskDTO1 = new TaskDTO();
		taskDTO1.setProjectId(project.getId());
		taskDTO1.setName("test1");
		@NotNull final TaskDTO taskDTO2 = new TaskDTO();
		taskDTO2.setProjectId(project.getId());
		taskDTO2.setName("test2");
		taskClient.addTask(userHeader, taskDTO);
		taskClient.addTask(userHeader, taskDTO1);
		taskClient.addTask(userHeader, taskDTO2);
		Assert.assertEquals(3, taskClient.getTasks(userHeader).size());
		Assert.assertEquals(0, taskClient.getTasks(adminHeader).size());
		taskClient.removeTask(userHeader, taskDTO.getId());
		taskClient.removeTask(userHeader, taskDTO1.getId());
		taskClient.removeTask(userHeader, taskDTO2.getId());
		Assert.assertEquals(0, taskClient.getTasks(userHeader).size());
	}

	@Test
	public void getTasksByProjectIdTest() {
		@NotNull final TaskDTO taskDTO = new TaskDTO();
		taskDTO.setProjectId(project.getId());
		taskDTO.setName("test");
		@NotNull final TaskDTO taskDTO1 = new TaskDTO();
		taskDTO1.setProjectId(project.getId());
		taskDTO1.setName("test1");
		taskClient.addTask(userHeader, taskDTO);
		taskClient.addTask(userHeader, taskDTO1);
		Assert.assertEquals(2, taskClient.getTasksByProjectId(userHeader, project.getId()).size());
		Assert.assertEquals(0, taskClient.getTasksByProjectId(adminHeader, project.getId()).size());
		taskClient.removeTask(userHeader, taskDTO.getId());
		taskClient.removeTask(userHeader, taskDTO1.getId());
		Assert.assertEquals(0, taskClient.getTasksByProjectId(userHeader, project.getId()).size());
	}

	@Test
	public void addTaskXMLTest() {
		@NotNull final TaskDTO taskDTO = new TaskDTO();
		taskDTO.setProjectId(project.getId());
		taskDTO.setName("test");
		@Nullable final TaskDTO created = taskClient.addTaskXML(userHeader, taskDTO);
		Assert.assertNotNull(created);
		Assert.assertEquals(taskDTO.getId(), created.getId());
		Assert.assertEquals(taskDTO.getName(), created.getName());
		taskClient.removeTask(userHeader, created.getId());
		Assert.assertNull(taskClient.getTaskXML(userHeader, created.getId()));
	}

	@Test
	public void updateTaskXMLTest() {
		@NotNull final TaskDTO taskDTO = new TaskDTO();
		taskDTO.setProjectId(project.getId());
		taskDTO.setName("test");
		@Nullable final TaskDTO created = taskClient.addTaskXML(userHeader, taskDTO);
		Assert.assertNotNull(created);
		created.setName("test2");
		created.setDescription("description");
		@Nullable final TaskDTO updated = taskClient.updateTaskXML(userHeader, created);
		Assert.assertNotNull(updated);
		Assert.assertEquals(created.getName(), updated.getName());
		Assert.assertEquals(created.getDescription(), updated.getDescription());
		Assert.assertNotNull(taskClient.getTaskXML(userHeader, created.getId()));
		taskClient.removeTask(userHeader, created.getId());
		Assert.assertNull(taskClient.getTaskXML(userHeader, created.getId()));
	}

	@Test
	public void getTasksXMLTest() {
		@NotNull final TaskDTO taskDTO = new TaskDTO();
		taskDTO.setProjectId(project.getId());
		taskDTO.setName("test");
		@NotNull final TaskDTO taskDTO1 = new TaskDTO();
		taskDTO1.setProjectId(project.getId());
		taskDTO1.setName("test1");
		@NotNull final TaskDTO taskDTO2 = new TaskDTO();
		taskDTO2.setProjectId(project.getId());
		taskDTO2.setName("test2");
		taskClient.addTaskXML(userHeader, taskDTO);
		taskClient.addTaskXML(userHeader, taskDTO1);
		taskClient.addTaskXML(userHeader, taskDTO2);
		Assert.assertEquals(3, taskClient.getTasksXML(userHeader).size());
		Assert.assertEquals(0, taskClient.getTasksXML(adminHeader).size());
		taskClient.removeTask(userHeader, taskDTO.getId());
		taskClient.removeTask(userHeader, taskDTO1.getId());
		taskClient.removeTask(userHeader, taskDTO2.getId());
		Assert.assertEquals(0, taskClient.getTasksXML(userHeader).size());
	}
	
}
