package ru.ahmetahunov.sp.integration;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.ahmetahunov.sp.dto.ProjectDTO;

public class ProjectRestIT extends AbstractRest {

	@After
	public void clean() {
		for (@NotNull final ProjectDTO projectDTO : projectClient.getProjects(userHeader)) {
			projectClient.removeProject(userHeader, projectDTO.getId());
		}
	}

	@Test
	public void addProjectTest() {
		@NotNull final ProjectDTO projectDTO = new ProjectDTO();
		projectDTO.setName("test");
		@Nullable final ProjectDTO created = projectClient.addProject(userHeader, projectDTO);
		Assert.assertNotNull(created);
		Assert.assertEquals(projectDTO.getId(), created.getId());
		Assert.assertEquals(projectDTO.getName(), created.getName());
		projectClient.removeProject(userHeader, created.getId());
		Assert.assertNull(projectClient.getProject(userHeader, created.getId()));
	}

	@Test
	public void updateProjectTest() {
		@NotNull final ProjectDTO projectDTO = new ProjectDTO();
		projectDTO.setName("test");
		@Nullable final ProjectDTO created = projectClient.addProject(userHeader, projectDTO);
		Assert.assertNotNull(created);
		created.setName("test2");
		created.setDescription("description");
		@Nullable final ProjectDTO updated = projectClient.updateProject(userHeader, created);
		Assert.assertNotNull(updated);
		Assert.assertEquals(created.getName(), updated.getName());
		Assert.assertEquals(created.getDescription(), updated.getDescription());
		Assert.assertNotNull(projectClient.getProject(userHeader, created.getId()));
		projectClient.removeProject(userHeader, created.getId());
		Assert.assertNull(projectClient.getProject(userHeader, created.getId()));
	}

	@Test
	public void removeProjectTest() {
		@Nullable final ProjectDTO projectDTO = new ProjectDTO();
		projectDTO.setName("test");
		Assert.assertNotNull(projectClient.addProject(userHeader, projectDTO));
		projectClient.removeProject(userHeader, projectDTO.getId());
		Assert.assertNull(projectClient.getProject(userHeader, projectDTO.getId()));
	}

	@Test
	public void getProjectsTest() {
		@NotNull final ProjectDTO projectDTO = new ProjectDTO();
		projectDTO.setName("test");
		@NotNull final ProjectDTO projectDTO1 = new ProjectDTO();
		projectDTO1.setName("test1");
		@NotNull final ProjectDTO projectDTO2 = new ProjectDTO();
		projectDTO2.setName("test2");
		projectClient.addProject(userHeader, projectDTO);
		projectClient.addProject(userHeader, projectDTO1);
		projectClient.addProject(userHeader, projectDTO2);
		Assert.assertEquals(3, projectClient.getProjects(userHeader).size());
		Assert.assertEquals(0, projectClient.getProjects(adminHeader).size());
		projectClient.removeProject(userHeader, projectDTO.getId());
		projectClient.removeProject(userHeader, projectDTO1.getId());
		projectClient.removeProject(userHeader, projectDTO2.getId());
		Assert.assertEquals(0, projectClient.getProjects(userHeader).size());
	}

	@Test
	public void addProjectXMLTest() {
		@NotNull final ProjectDTO projectDTO = new ProjectDTO();
		projectDTO.setName("test");
		@Nullable final ProjectDTO created = projectClient.addProjectXML(userHeader, projectDTO);
		Assert.assertNotNull(created);
		Assert.assertEquals(projectDTO.getId(), created.getId());
		Assert.assertEquals(projectDTO.getName(), created.getName());
		projectClient.removeProject(userHeader, created.getId());
		Assert.assertNull(projectClient.getProjectXML(userHeader, created.getId()));
	}

	@Test
	public void updateProjectXMLTest() {
		@NotNull final ProjectDTO projectDTO = new ProjectDTO();
		projectDTO.setName("test");
		@Nullable final ProjectDTO created = projectClient.addProjectXML(userHeader, projectDTO);
		Assert.assertNotNull(created);
		created.setName("test2");
		created.setDescription("description");
		@Nullable final ProjectDTO updated = projectClient.updateProjectXML(userHeader, created);
		Assert.assertNotNull(updated);
		Assert.assertEquals(created.getName(), updated.getName());
		Assert.assertEquals(created.getDescription(), updated.getDescription());
		Assert.assertNotNull(projectClient.getProjectXML(userHeader, created.getId()));
		projectClient.removeProject(userHeader, created.getId());
		Assert.assertNull(projectClient.getProjectXML(userHeader, created.getId()));
	}

	@Test
	public void getProjectsXMLTest() {
		@NotNull final ProjectDTO projectDTO = new ProjectDTO();
		projectDTO.setName("test");
		@NotNull final ProjectDTO projectDTO1 = new ProjectDTO();
		projectDTO1.setName("test1");
		@NotNull final ProjectDTO projectDTO2 = new ProjectDTO();
		projectDTO2.setName("test2");
		projectClient.addProjectXML(userHeader, projectDTO);
		projectClient.addProjectXML(userHeader, projectDTO1);
		projectClient.addProjectXML(userHeader, projectDTO2);
		Assert.assertEquals(3, projectClient.getProjectsXML(userHeader).size());
		Assert.assertEquals(0, projectClient.getProjectsXML(adminHeader).size());
		projectClient.removeProject(userHeader, projectDTO.getId());
		projectClient.removeProject(userHeader, projectDTO1.getId());
		projectClient.removeProject(userHeader, projectDTO2.getId());
		Assert.assertEquals(0, projectClient.getProjectsXML(userHeader).size());
	}

}
