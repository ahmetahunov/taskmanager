package ru.ahmetahunov.sp.integration;

import org.jetbrains.annotations.NotNull;
import org.springframework.util.Base64Utils;
import ru.ahmetahunov.sp.client.ProjectClient;
import ru.ahmetahunov.sp.client.TaskClient;
import ru.ahmetahunov.sp.client.UserClient;

public abstract class AbstractRest {

	@NotNull
	private static final String url = "http://localhost:8080/";

	@NotNull
	protected static final ProjectClient projectClient = ProjectClient.client(url);

	@NotNull
	protected static final TaskClient taskClient = TaskClient.client(url);

	@NotNull
	protected static final UserClient userClient = UserClient.client(url);

	@NotNull
	protected static final String adminHeader = authorization("admin", "admin");

	@NotNull
	protected static final String userHeader = authorization("user", "user");

	private static String authorization(@NotNull final String login, @NotNull final String password) {
		byte[] encodedBytes = Base64Utils.encode((login + ":" + password).getBytes());
		return "Basic " + new String(encodedBytes);
	}

}
