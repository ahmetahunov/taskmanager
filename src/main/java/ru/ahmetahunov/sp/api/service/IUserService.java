package ru.ahmetahunov.sp.api.service;

import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.sp.entity.User;

public interface IUserService extends IAbstractService<User> {

	@Nullable
	public User findByLogin(String login);

}
