package ru.ahmetahunov.sp.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.QueryHints;
import ru.ahmetahunov.sp.entity.Task;
import javax.persistence.QueryHint;
import java.util.List;

public interface TaskRepository extends JpaRepository<Task, String> {

    @NotNull
    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    public List<Task> findAllTasksByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

    @NotNull
    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    public List<Task> findAllByUserId(@NotNull final String userId);

    @Nullable
    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    public Task findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    public void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}
